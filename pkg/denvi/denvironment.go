package denvi

import (
	"errors"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strings"

	"gitlab.com/denvi/denvi/pkg/utils/regex"
	"gopkg.in/yaml.v2"
)

const PathRegex = `(?P<domain>[\w\d]+\.{1}[\w\d]+(?:\:[\d]+)?\/{1})?(?P<name>[\w\d\/\-]+){1}(?:(?:\:){1}(?P<version>[\w\d\.\-]+))?`

type Denvibuild struct {
	File      string            `json:"file" yaml:"file"`
	Arguments map[string]string `json:"arguments" yaml:"arguments"`
}

type Denvipoint struct {
	Image string     `json:"image" yaml:"image"`
	Build Denvibuild `json:"build" yaml:"build"`
}

type Denvironment struct {
	Path    string
	Version string
	Hoster  string

	Entries  map[string]Denvipoint
	IsRemote bool
}

func GetDenvironment(path string) (*Denvironment, error) {
	// check wether path is remote or local
	if !strings.HasPrefix(path, ".") && !strings.HasPrefix(path, "/") {
		// parse repository info from path (hoster, version)
		rpath := parsePath(path)
		denvinitionUrl := "https://" + rpath["domain"] + rpath["name"] + "/raw/" + rpath["version"] + "/denvironment.yml"
		// validate remote repository (check for denvironment.yml)
		denvinition, err := getRemoteDenvinition(denvinitionUrl)
		if err != nil {
			return nil, err
		}
		// return denvironment information
		denvironment := Denvironment{
			Hoster:   rpath["domain"],
			Version:  rpath["version"],
			IsRemote: true,
		}

		if err = parseDenvinition(denvinition, &denvironment); err != nil {
			return nil, err
		}

		return &denvironment, nil
	} else {
		// validate local repository (check for denvironment.yml)
		if _, err := os.Stat(path); err != nil {
			return nil, err
		} else if _, err := os.Stat(path + "/denvironment.yml"); err != nil {
			return nil, err
		}

		denvinition, err := ioutil.ReadFile(path + "/denvironment.yml")
		if err != nil {
			return nil, err
		}
		// parse repository info from git (hoster, version)
		// return denvironment information

		denvironment := Denvironment{
			Hoster:   "local",
			Version:  "main",
			IsRemote: false,
		}

		if err = parseDenvinition(denvinition, &denvironment); err != nil {
			return nil, err
		}

		return &denvironment, nil
	}
}

var providers = []string{
	"github.com/",
	"gitlab.com/",
	"bitbucket.org/",
}

func guessHoster(name string) (string, error) {
	for _, provider := range providers {
		resp, err := http.Get("https://" + provider + name)
		if err == nil && resp.StatusCode == 200 {
			return provider, nil
		}
	}

	return "", errors.New("Couldn't guess hoster! Please provide a full URL with domain!")
}

func getRemoteDenvinition(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return nil, errors.New("Invalid Denvironment: denvironment.yml is missing (" + url + ")")
	}
	defer resp.Body.Close()

	denvinition, err := io.ReadAll(resp.Body)

	if err != nil {
		return nil, errors.New("Invalid Denvironment: denvironment.yml is missing (" + url + ")")
	}

	return denvinition, nil
}

func parsePath(path string) map[string]string {
	// parse denvironment uri from path
	rPath := &regex.Regex{regexp.MustCompile(PathRegex)}
	gPath := rPath.FindNamedGroups(path)

	// check if domain is specified
	if len(gPath["domain"]) == 0 {
		// try to guess hoster from common ones
		gPath["domain"], _ = guessHoster(gPath["name"])
	}

	// check if version is specified
	if len(gPath["version"]) == 0 {
		gPath["version"] = "main"
	}

	return gPath
}

func parseDenvinition(denvinition []byte, denvironment *Denvironment) error {
	// parse definition data
	var data map[string]Denvipoint
	if err := yaml.Unmarshal(denvinition, &data); err != nil {
		return err
	}

	if denvironment.Entries == nil {
		denvironment.Entries = make(map[string]Denvipoint)
	}
	// parse denvironment entries
	for entry, config := range data {
		if entry[0] != '.' {
			denvironment.Entries[entry] = config
		}
	}

	return nil
}
