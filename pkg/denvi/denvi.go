package denvi

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"strings"

	"github.com/fatih/color"
	"github.com/rodaine/table"
)

type Denvi struct {
	Environments []Denvironment
}

func NewDenvi() *Denvi {
	return &Denvi{}
}

func (d *Denvi) LoadLocalDenvironments() {
	localRepo := "./denvs"
	items, err := ioutil.ReadDir(localRepo)

	for _, info := range items {
		if err != nil || !info.IsDir() {
			continue
		}

		if _, err := os.Stat(localRepo + "/" + info.Name() + "/denvironment.yml"); errors.Is(err, os.ErrNotExist) {
			continue
		}

		denv, err := GetDenvironment(localRepo + "/" + info.Name() + "/")
		if err != nil {
			fmt.Println(err.Error())
			continue
		}

		denv.Path = info.Name()
		d.Environments = append(d.Environments, *denv)
	}
}

func stringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

func (d *Denvi) List(remote bool) {
	if d.Environments == nil || len(d.Environments) == 0 {
		d.LoadLocalDenvironments()
	}

	headerFmt := color.New(color.FgBlue, color.Bold).SprintfFunc()

	tbl := table.New("Path", "Hoster", "Version", "Entries", "Images").WithHeaderFormatter(headerFmt)

	for _, denv := range d.Environments {
		var entries []string
		var images []string
		for key, info := range denv.Entries {
			entries = append(entries, key)
			if !stringInSlice(info.Image, images) {
				images = append(images, info.Image)
			}
		}

		tbl.AddRow(
			denv.Path,
			denv.Hoster,
			denv.Version,
			strings.Join(entries, ", "),
			strings.Join(images, ", "),
		)
	}

	tbl.Print()
}

func (d *Denvi) Pull(slug string) {
	denv, err := GetDenvironment(slug)
	if err != nil {
		log.Fatalln(err)
	}

	fmt.Println(denv.IsRemote)
}

func (d *Denvi) Use(slug string) {}

func (d *Denvi) Remove(slug string) {}
