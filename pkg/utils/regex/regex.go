package regex

import "regexp"

type Regex struct {
	*regexp.Regexp
}

func (r *Regex) FindNamedGroups(str string) map[string]string {
	match := r.FindStringSubmatch(str)
	result := make(map[string]string)
	for i, name := range r.SubexpNames() {
		if i != 0 && name != "" {
			result[name] = match[i]
		}
	}

	return result
}
