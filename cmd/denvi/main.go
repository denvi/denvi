package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"
	"gitlab.com/denvi/denvi/pkg/denvi"
)

func CreateDenviCLI() cli.App {
	denvi := denvi.NewDenvi()
	app := &cli.App{
		Name:        "Denvi",
		Description: "Local container development environments",
		Usage:       "Manage local development environments",
		Commands: []*cli.Command{
			{
				Name:    "list",
				Aliases: []string{"ls"},
				Usage:   "List environments",
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:  "local",
						Usage: "only list local environments",
					},
				},
				Action: func(c *cli.Context) error {
					denvi.List(c.Bool("local"))
					return nil
				},
			},
			{
				Name:  "pull",
				Usage: "Download an environment",
				Action: func(c *cli.Context) error {
					if c.Args().Len() == 0 {
						fmt.Println("Please provide an environment slug!")
						return nil
					}

					denvi.Pull(c.Args().First())
					return nil
				},
			},
			{
				Name:  "use",
				Usage: "Activate environment",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "image",
						Usage: "Use custom image",
					},
					&cli.StringFlag{
						Name:  "buildfile",
						Usage: "Use a custom build/dockerfile",
					},
				},
				Action: func(c *cli.Context) error {
					if c.Args().Len() == 0 {
						fmt.Println("Please provide an environment slug!")
						return nil
					}

					denvi.Use(c.Args().First())
					return nil
				},
			},
			{
				Name:    "remove",
				Aliases: []string{"rm"},
				Flags: []cli.Flag{
					&cli.BoolFlag{
						Name:  "purge",
						Usage: "remove all data (volumes) too",
					},
				},
			},
			{
				Name: "cli",
				Subcommands: []*cli.Command{
					{
						Name:  "run",
						Usage: "run a specific environment",
					},
					{
						Name:  "create",
						Usage: "create a specific environment cli proxy",
						Flags: []cli.Flag{
							&cli.StringFlag{
								Name:    "version",
								Aliases: []string{"v"},
								Usage:   "version of environment to proxy",
							},
						},
					},
				},
			},
		},
	}
	return *app
}

func main() {
	app := CreateDenviCLI()
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}
