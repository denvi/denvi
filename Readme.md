Preconfigured development environments using containers with native bindings.

Think of this as [NVM]() or [PyEnv](), just framework/language agnostic and containerized.

# install

## binary

1. download [Denvi]() from the latest release and place it somewhere in your `$PATH`
2. initialize: `denvi init`
3. Verify installation: `denvi --version`

## source

> [Podman]() is needed to build the source using the provided build script!

1. `git clone https://gitlab.com/denvi/denvi ~/denvi`
2. `cd ~/denvi && ./build.sh`

# updating

Updating denvi is as simple as running `denvi self-update`. This will download, build and install the latest denvi version. 

# usage

- pull denvironment: `denvi pull <path>@<version>`
- activate denvironment: `denvi use <path>@<version>`
- pull & activate at once: `denvi install <path>@<version>`
- remove/uninstall denvironment: `denvi rm <path>@<version>`


# denvironments

Denvironments ( = *Development Environments*) are the foundation of denvi and provide scripts and files which are needed to bootstrap a specific environment in podman.

Denvironments are designed primary to make installation of native packages obsolete and provide a robust development expirience.

A denvironment is typicaly provided with a public git repository (like GitHub or GitLab) which exposes some information about the environment and their bootstrapping.

## structure

The only required file for a denvironment repository is the `[name].denvi.yml` which defines the denvironment itself.

Beside the configuration file, you can also provide a `build` directory containing either dockerfiles/containerfiles or buildah scripts. This is especially useful when customizing the denvironment.

### `[name].denvi.yml`

For example (`php.denvi.yml`):

```yaml
exposes:
   - php
   - composer
```